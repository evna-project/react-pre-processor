const babel = require("@babel/core");

interface IBabelOption {
    presets?: string[] | Array<any>,
    plugins?: string[] | Array<any>
}

const babelDefaultOptions: {
    [key: string]: IBabelOption
} = {
    jsx: {},
    tsx: {
        presets: [],
        plugins: [
            ['@babel/plugin-transform-typescript', { isTSX: true }]
        ]
    }
}

export const compile = (code: string, type: string | null) => {
    if (!type || /jsx?/g.test(type)) type = 'jsx';
    if (/tsx?/g.test(type)) type = 'tsx';

    const options: IBabelOption | undefined = babelDefaultOptions[type];

    if (!options) {
        return new Error(`${type} type is not compatible`);
    }

    // If it's jsx so that's what we're looking for
    if (type === 'jsx') return code;

    // Otherwise it will be transformed
    const transformed = babel.transform(code, options);
    return transformed.code;
}