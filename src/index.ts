/// <reference path="./index.d.ts" />

import parser from '../../parser/src'

//--------------------------
import { readFileSync } from 'fs'
import { compile } from './script-processor/babelTransformer';
const sample = readFileSync('test/sample.component', 'utf8')
//--------------------------


const parseScript = (name: string, script: IBlockResult) => {
  const { type, props, content } = script;
  const result = compile(content, type)
  return result;
}

const toReact = (parsedComponent: IParsedData) => {
  const { name, imports, template, scripts, styles } = parsedComponent;
  const jsx = scripts.map(script => parseScript(name, script))

  console.log(jsx)
}

const component = toReact(parser(sample))